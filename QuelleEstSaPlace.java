import java.util.Scanner;

public class QuelleEstSaPlace {

    public static String adjNumOrd(int num) {
        if (num < 0) {
            throw new IllegalArgumentException("Le paramètre doit être un entier naturel strictement positif");
        } else if (num == 1) {
            return "1ère";
        } else {
            return num + "ème";
        }
    }

    public static void whereHeIs() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Entrer une phrase : ");
        String txt = scanner.nextLine().toLowerCase();

        System.out.print("Entrer un caractère à rechercher : ");
        String car = scanner.nextLine().toLowerCase();

        int i = 0;

        for (int pos = 0; pos < txt.length(); pos++) {
            if (txt.charAt(pos) == car.charAt(0)) {
                i++;
                System.out.println("On retrouve le caractère " + car + " à la " + adjNumOrd(pos + 1) + " position");
            }
        }

        if (i == 0) {
            System.out.println("Le caractère n'est pas présent dans la phrase.");
        }
        
        scanner.close();
    }

    public static void main(String[] args) {
        whereHeIs();
    }
}