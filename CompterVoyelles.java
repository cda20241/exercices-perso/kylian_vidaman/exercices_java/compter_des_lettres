import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CompterVoyelles {
    public static void main(String[] args) {
        char[] listeVoyelle = {'a', 'e', 'o', 'u', 'y'};
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ecrire une phrase : ");
        String texte = scanner.nextLine();

        Map<Character, Integer> showVoyelle = new HashMap<>();
        int compteur = 0;

        for (char voyelle : listeVoyelle) {
            int value = 0;
            for (char lettre : texte.toCharArray()) {
                if (lettre == voyelle) {
                    compteur++;
                    value++;
                }
            }
            if (value > 0) {
                showVoyelle.put(voyelle, value);
            }
        }

        System.out.println("Il y a " + compteur + " voyelles dans la phrase");
        System.out.println(showVoyelle);
        scanner.close();
    }
}
